#include "Triangle.hpp"
#include "Plan.hpp"



Triangle::Triangle() : Objet(){
  s[0].set(-1, 0, -1);
  s[1].set(1, 0, -1);
  s[2].set(0, 0, 1);
  n.set(0, 0, 1);
}

Triangle::Triangle(const Point p[3], Materiau m) : Objet(m) {
  for(int i=0; i<3; i++)
    s[i] = p[i];
    float v1p0p1 = p[0].Y*p[1].Z - p[0].Z*p[1].Y;
    float v2p0p1 = p[0].Z*p[1].X - p[0].X*p[1].Z;
    float v3p0p1 = p[0].X*p[1].Y - p[0].Y*p[1].X;

    float v1p0p2 = p[0].Y*p[2].Z - p[0].Z*p[2].Y;
    float v2p0p2 = p[0].Z*p[2].X - p[0].X*p[2].Z;
    float v3p0p2 = p[0].X*p[2].Y - p[0].Y*p[2].X;
  // calcul de la normale à partir du produit vectoriel AB^AC
  // cette normale doit ensuite être normalisée...

}

Triangle::~Triangle(){}


bool Triangle::intersecte(const Rayon& r, Intersection& inter){

  return false;
  
}

bool Triangle::coupe(const Rayon& r){
  return false;
}


Vecteur Triangle::getNormale(const Point &p){
  return n;
}

ostream& operator<<(ostream & sortie, Triangle & t){
  sortie << "triangle : ";
  for(int i=0; i<3; i++)
    sortie << t.s[i] << " - ";
  sortie << endl;

  return sortie;
    
}

void Triangle::affiche(ostream& out){
  out << "triangle : ";
  for(int i=0; i<3; i++)
    out << s[i] << " - ";
  out << endl;
}

