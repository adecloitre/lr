var class_objet =
[
    [ "Objet", "class_objet.html#aefdd826d50085897e4894ffef4597d04", null ],
    [ "Objet", "class_objet.html#a085607e76558af55f08640b92c713abd", null ],
    [ "~Objet", "class_objet.html#a8d9fa6fa05b9f5a99ab60d08f6b558ee", null ],
    [ "affiche", "class_objet.html#af87b1bdaf76f047a2c7684ebfcf96078", null ],
    [ "calculerAmbiant", "class_objet.html#a691fe4ecedb784b7cbcd90ed578f496f", null ],
    [ "calculerDirect", "class_objet.html#a8b62b316915f8c648678045ae2f0c28e", null ],
    [ "coupe", "class_objet.html#af536ecd30fc676ce907b36da66a713c7", null ],
    [ "getCouleur", "class_objet.html#ad9945443be39a74a34647eb1fd581263", null ],
    [ "getNormale", "class_objet.html#a1ea27651c4d4380717a8fbb82f0194d3", null ],
    [ "getReflechi", "class_objet.html#ad476f009b5b4702eb1395140191c8687", null ],
    [ "intersecte", "class_objet.html#ad6454ac01678ac31f1236b34377af6ff", null ],
    [ "isSpecular", "class_objet.html#a1f0a140bbe2573edc5a597b4fd35eec8", null ],
    [ "operator<<", "class_objet.html#aca38d9bf8e0f383b48881d8e3763b84d", null ],
    [ "mat", "class_objet.html#a3041fea9f7d29506fda220733dde4e2d", null ]
];