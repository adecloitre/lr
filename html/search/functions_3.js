var searchData=
[
  ['image_0',['Image',['../class_image.html#a58edd1c45b4faeb5f789b0d036d02313',1,'Image::Image()'],['../class_image.html#ae45ff6f57e9b33c43232944809918a58',1,'Image::Image(int l, int h)']]],
  ['intensite_1',['Intensite',['../class_intensite.html#a330274da47646af0867c9320cff5cc4f',1,'Intensite::Intensite()'],['../class_intensite.html#adbbb0bbc0c1f31fe65e38c1e1024fa51',1,'Intensite::Intensite(float r, float v, float b)']]],
  ['intersecte_2',['intersecte',['../class_objet.html#ad6454ac01678ac31f1236b34377af6ff',1,'Objet::intersecte()'],['../class_plan.html#ad3cf4c0761f5eb1076149e1a96bb3ea2',1,'Plan::intersecte()'],['../class_scene.html#a697a6d5af15f92ebe663ca450a1d6718',1,'Scene::intersecte()'],['../class_sphere.html#aaa5548e7899cdf98049ea5e104f5cc0e',1,'Sphere::intersecte()']]],
  ['intersection_3',['Intersection',['../class_intersection.html#a67497e3efe2793b23909052eeb82c4f3',1,'Intersection::Intersection()'],['../class_intersection.html#a7f61bdebf3f686aba2347d71624fc675',1,'Intersection::Intersection(const Point &amp;p, Objet *o, const float &amp;t)']]],
  ['isspecular_4',['isSpecular',['../class_objet.html#a1f0a140bbe2573edc5a597b4fd35eec8',1,'Objet']]]
];
