var indexSectionsWithContent =
{
  0: "abcdefghiklmnoprsvxyz~",
  1: "cimoprsv",
  2: "cilmoprsv",
  3: "acgimnoprsv~",
  4: "abcdfhiklmoprsvxyz",
  5: "o",
  6: "ep"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "related",
  6: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Friends",
  6: "Macros"
};

