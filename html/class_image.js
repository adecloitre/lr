var class_image =
[
    [ "Image", "class_image.html#a58edd1c45b4faeb5f789b0d036d02313", null ],
    [ "Image", "class_image.html#ae45ff6f57e9b33c43232944809918a58", null ],
    [ "~Image", "class_image.html#a0294f63700543e11c0f0da85601c7ae5", null ],
    [ "getHauteur", "class_image.html#a1c31924f90bff0d36c65d61fde17ad86", null ],
    [ "getLargeur", "class_image.html#ad20b4bb98d4bb38d66ff372672f2174b", null ],
    [ "getPixel", "class_image.html#a91fbc9601d60cd0ea984399652bc71b8", null ],
    [ "sauver", "class_image.html#a892da84c19b6fd186d48c5663d9c8f43", null ],
    [ "setPixel", "class_image.html#a74dd1d2211a6f4766524f7d89bb706a9", null ],
    [ "hauteur", "class_image.html#acf4ec3bdaf15c5f3f32d31f7b7b1fcce", null ],
    [ "largeur", "class_image.html#a957c831b42b7e8a8499179574f911749", null ],
    [ "pixel", "class_image.html#a55b39855c7b72502c9a09e8caefbe603", null ]
];